json.array!(@people) do |person|
  json.extract! person, :id, :name, :weight, :height, :float, :color, :education, :place, :of, :birth
  json.url person_url(person, format: :json)
end
