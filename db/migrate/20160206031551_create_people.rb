class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :name
      t.float :weight
      t.string :height
      t.string :float
      t.string :color
      t.string :education
      t.string :place
      t.string :of
      t.string :birth

      t.timestamps null: false
    end
  end
end
